import { Component, OnInit } from '@angular/core';
import { FriendService } from '../../services/friend.service';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';

export interface TokenObject{
  success:boolean
}

export interface FriendResponseObject{
  success:boolean,
  error?:string,
  message?:string
}

export interface FriendReq{
  first_name:string,
  last_name:string,
  email:string,
  profile_pic:string,
  id:number
}

export interface FriendReqs{
  success:boolean,
  error?:string,
  friend_req?:FriendReq[]
}

@Component({
  selector: 'app-friend',
  templateUrl: './friend.component.html',
  styleUrls: ['./friend.component.css']
})
export class FriendComponent implements OnInit {

  private friend_req:FriendReq[];

  constructor(
    private authService:AuthenticationService,
    private router:Router,
    private friendService:FriendService
  ) { }

  ngOnInit() {
    if(this.authService.checkTokenExists()){
      this.authService.checkToken().subscribe((data:TokenObject)=>{
        if(!data.success){
          this.router.navigate(['/auth']);
        }else{
          this.GetFriendReq();
        }
      });
    }else{
      this.router.navigate(['/auth']);
    }
  }

  GetFriendReq(){
    this.friend_req = undefined;
    const token = this.authService.getToken();
    this.friendService.GetFriendReq(token,0).subscribe((data:FriendReqs)=>{
      if(data.success){
        this.friend_req = data.friend_req;
      }else{
        alert(data.error);
      }
    });
  }

  AcceptRequest(friend_req:FriendReq){
    const token = this.authService.getToken();
    this.friendService.AcceptReq(token,friend_req.id).subscribe((data:FriendResponseObject)=>{
      if(data.success){
        alert('Friend Request Accepted');
        this.GetFriendReq();
      }else{
        alert(data.error);
      }
    });
  }

  DeclineRequest(friend_req:FriendReq){
    const token = this.authService.getToken();
    this.friendService.DeclineReq(token,friend_req.id).subscribe((data:FriendResponseObject)=>{
      if(data.success){
        alert('Friend Request Declined');
        this.GetFriendReq();
      }else{
        alert(data.error);
      }
    });
  }

}
