import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ProfileService } from '../../services/profile.service';
import { PictureService } from '../../services/picture.service';

export interface Pic{
  pic_url:string,
  id:number
};

export interface Pics{
  pic:Pic[]
}

export interface PO{
  success:boolean,
  error?:string,
  results?:Pics[]
}

export interface TokenObject{
  success:boolean
}

export interface ProfileAbout{
  success:boolean,
  error?:string,
  first_name?:string,
  last_name?:string,
  email?:string,
  id?:number,
  profile_pic?:string
}

export interface ResponseObject{
  success:boolean,
  error?:string,
  message?:string
}

@Component({
  selector: 'app-picture',
  templateUrl: './picture.component.html',
  styleUrls: ['./picture.component.css']
})
export class PictureComponent implements OnInit {

  private email:string;
  private pics:Pics[];
  private myprofile:boolean=false;
  private view_image:boolean=false;
  private img_id:number;
  private img_url:string;
  //private img_title:string;

  constructor(
    private authService:AuthenticationService,
    private router:Router,
    private activatedRoute:ActivatedRoute,
    private profileService:ProfileService,
    private picService:PictureService
  ) { }

  ngOnInit() {
    if(this.authService.checkTokenExists()){
      this.authService.checkToken().subscribe((data:TokenObject)=>{
        if(!data.success){
          this.router.navigate(['/auth']);
        }else{
          this.activatedRoute.parent.paramMap.subscribe(p=>{
            this.email=p.get('email');
            if(this.email=="me"){
              this.myprofile=true;
            }else{
              this.myprofile=false;
            }
            const token = this.authService.getToken();
            this.profileService.GetProfileInfo(token,this.email).subscribe((data:ProfileAbout)=>{
              if(data.success){
                const token = this.authService.getToken();
                this.picService.GetUserPictures(token,data.email,0).subscribe((d:PO)=>{
                  console.log(d);
                  if(d.success){
                    this.pics = d.results;
                    console.log(this.pics);
                  }else{
                    alert(d.error);
                  }
                });
              }else{
                alert(data.error);
              }
            });
          });
        }
      });
    }else{
      this.router.navigate(['/auth']);
    }
  }

  ViewImage(p:Pic){
    console.log(p);
    this.view_image=true;
    this.img_id = p.id;
    this.img_url = p.pic_url;
  }

  CloseModal(){
    this.view_image=false;
  }

  GetImages(){
    this.pics=undefined
    const token = this.authService.getToken();
    this.profileService.GetProfileInfo(token,this.email).subscribe((data:ProfileAbout)=>{
      if(data.success){
        const token = this.authService.getToken();
        this.picService.GetUserPictures(token,data.email,0).subscribe((d:PO)=>{
          console.log(d);
          if(d.success){
            this.pics = d.results;
            console.log(this.pics);
          }else{
            alert(d.error);
          }
        });
      }else{
        alert(data.error);
      }
    });
  }

  DeleteImage(){
    const id = this.img_id;
    const token = this.authService.getToken();
    this.picService.DeleteImage(token,id).subscribe((data:ResponseObject)=>{
      if(data.success){
        this.CloseModal();
        this.GetImages();
      }else{
        alert(data.error);
      }
    });
  }

  /*UpdateImage(){

  }*/

  SetProfilePic(){
    const id = this.img_id;
    const token = this.authService.getToken();
    this.picService.SetProfilePic(token,id).subscribe((data:ResponseObject)=>{
      if(data.success){
        this.router.navigate(['/home/profile/'+this.email]);
      }else{
        alert(data.error);
      }
    });
  }

}
