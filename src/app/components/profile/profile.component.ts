import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ProfileService } from '../../services/profile.service';

export interface TokenObject{
  success:boolean
}

export interface ProfileAbout{
  success:boolean,
  error?:string,
  first_name?:string,
  last_name?:string,
  email?:string,
  id?:number,
  profile_pic?:string
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  private email:string;
  private first_name:string;
  private last_name:string;
  private posts:boolean = false;
  private pic:boolean = false;
  private about:boolean = false;
  private friend:boolean = false;
  private profile_pic:string;

  constructor(
    private authService:AuthenticationService,
    private router:Router,
    private activatedRoute:ActivatedRoute,
    private profileService:ProfileService
  ) { }

  ngOnInit() {
    if(this.authService.checkTokenExists()){
      this.authService.checkToken().subscribe((data:TokenObject)=>{
        if(!data.success){
          this.router.navigate(['/auth']);
        }else{
          this.activatedRoute.paramMap.subscribe(params=>{
            this.email = params.get('email');
            const token = this.authService.getToken();
            this.profileService.GetProfileInfo(token,this.email).subscribe((data:ProfileAbout)=>{
              if(data.success){
                this.first_name = data.first_name;
                this.last_name = data.last_name;
                this.profile_pic = data.profile_pic;
              }else{
                alert(data.error);
              }
            });
          });
        }
      });
    }else{
      this.router.navigate(['/auth']);
    }
  }

  ViewPosts(){
    this.posts = true;
    this.about = false;
    this.pic = false;
    this.friend = false;
    this.router.navigate(['/home/profile/'+this.email+'/post']);
  }

  ViewAbout(){
    this.posts = false;
    this.about = true;
    this.pic = false;
    this.friend = false;
    this.router.navigate(['/home/profile/'+this.email+'/about']);
  }

  ViewPictures(){
    this.posts = false;
    this.about = false;
    this.pic = true;
    this.friend = false;
    this.router.navigate(['/home/profile/'+this.email+'/picture']);
  }

  ViewFriends(){
    this.posts = false;
    this.about = false;
    this.pic = false;
    this.friend = true;
    this.router.navigate(['/home/profile/'+this.email+'/friends']);
  }

}
