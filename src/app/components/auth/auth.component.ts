import { Component, Renderer2, OnInit, OnDestroy } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';

export interface TokenObject{
  success:boolean
}

export interface LogOutObject{
  success:boolean,
  message?:string,
  error?:string
}

export interface UserObject{
  id:number,
  first_name:string,
  last_name:string,
  email:string
}

export interface LoginObject{
  success:boolean,
  error?:string,
  token?:string,
  user?:UserObject,
  message?:string
}

export interface RegisterObject{
  success:boolean,
  error?:string,
  message?:string
}

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})

export class AuthComponent implements OnInit {
  private isCollapsed: boolean =true;
  private log_email: string;
  private log_password: string;

  private reg_email: string;
  private reg_password: string;
  private reg_confirm_password: string;
  private reg_first_name: string;
  private reg_last_name: string;

  constructor(
    private validateService: ValidateService,
    private authService: AuthenticationService,
    private router: Router,
    private renderer2: Renderer2
  ) {
    if(this.authService.checkTokenExists()){
      this.authService.checkToken().subscribe((data:TokenObject)=>{
        if(data.success){
          this.router.navigate(['/home/post/all']);
        }
      });
    }
  }

  ngOnInit() {
    this.renderer2.addClass(document.body.parentElement,'bg-secondary');
    this.renderer2.addClass(document.body,'bg-secondary');
  }

  ngOnDestroy(){
    this.renderer2.removeClass(document.body.parentElement,'bg-secondary');
    this.renderer2.removeClass(document.body,'bg-secondary');
  }

  onRegisterSubmit(){
    const user = {
      email: this.reg_email,
      first_name: this.reg_first_name,
      last_name: this.reg_last_name,
      password: this.reg_password,
      confirm_password:this.reg_confirm_password
    };
    this.reg_email = undefined;
    this.reg_first_name = undefined;
    this.reg_last_name = undefined;
    this.reg_password = undefined;
    this.reg_confirm_password = undefined;
    if(!this.validateService.validateRegister(user)){
      return false;
    }

    this.authService.registerUser(user).subscribe((data:RegisterObject)=>{
      if(data.success){
        alert('Registered You May Now Log In');
      }else{
        alert(data.error);
      }
    });
  }

  onLogOut(){
    if(this.authService.checkTokenExists){
      this.authService.checkToken().subscribe((data:TokenObject)=>{
        if(data.success){
          this.authService.logOut().subscribe((data:LogOutObject)=>{
            if(data.success){
              this.authService.destroyCookie();
              this.router.navigate(['/auth']);
            }else{
              alert(data.error);
            }
          });
        }else{
          alert('Invalid Token');
          this.authService.destroyCookie();
          this.router.navigate(['/auth']);
        }
      });
    }else{
      alert('Unexistant Token');
      this.router.navigate(['/auth']);
    }
  }

  onLoginSubmit(){
    const user = {
      email:this.log_email,
      password:this.log_password
    };

    this.log_email = undefined;
    this.log_password = undefined;

    if(!this.validateService.validateLogin(user)){
      return false;
    }
    if(this.authService.checkTokenExists()){
      this.authService.checkToken().subscribe((data:TokenObject)=>{
        console.log(data);
        if(data.success){
          alert('Already Logged In');
          return false;
        }else{
          this.authService.loginUser(user).subscribe((data:LoginObject)=>{
            console.log('login: '+data);
            if(data.success){
              this.authService.destroyCookie();
              this.authService.setToken(data.token);
              this.router.navigate(['/home/post/all']);
            }else{
              alert(data.error);
            }
          });
        }
      });
    }else{
      this.authService.loginUser(user).subscribe((data:LoginObject)=>{
        console.log(data);
        if(data.success){
          this.authService.setToken(data.token);
          this.router.navigate(['/home/post/all']);
        }else{
          alert(data.error);
        }
      });
    }
  }

}
