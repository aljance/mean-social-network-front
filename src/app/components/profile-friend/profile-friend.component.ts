import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FriendService } from '../../services/friend.service';
import { ProfileService } from '../../services/profile.service';

export interface TokenObject{
  success:boolean
}

export interface FriendResponseObject{
  success:boolean,
  error?:string,
  message?:string
}

export interface SearchResultObject{
  first_name:string,
  last_name:string,
  id:number,
  email:string,
  profile_pic:string
}

export interface ProfileAbout{
  success:boolean,
  error?:string,
  first_name?:string,
  last_name?:string,
  email?:string,
  id?:number,
  profile_pic?:string
}

export interface SearchResultObjects{
  success:boolean,
  error?:string,
  results?:SearchResultObject[]
}

@Component({
  selector: 'app-profile-friend',
  templateUrl: './profile-friend.component.html',
  styleUrls: ['./profile-friend.component.css']
})
export class ProfileFriendComponent implements OnInit {

  private myprofile:boolean=false;
  private friends:SearchResultObject[];
  private email:string;

  constructor(
    private authService:AuthenticationService,
    private router:Router,
    private activatedRoute:ActivatedRoute,
    private friendService:FriendService,
    private profileService:ProfileService
  ) { }

  ngOnInit() {
    if(this.authService.checkTokenExists()){
      this.authService.checkToken().subscribe((data:TokenObject)=>{
        if(!data.success){
          this.router.navigate(['/auth']);
        }else{
          this.activatedRoute.parent.paramMap.subscribe(p=>{
            const email = p.get('email');
            if(email=="me"){
              this.myprofile=true;
            }else{
              this.myprofile=false;
            }
            const token = this.authService.getToken();
            this.profileService.GetProfileInfo(token,email).subscribe((data:ProfileAbout)=>{
              if(data.success){
                this.friendService.GetFriends(token,data.email,0).subscribe((data:SearchResultObjects)=>{
                  console.log(data);
                  if(data.success){
                    this.friends = data.results;
                  }else{
                    alert(data.error);
                  }
                });
              }else{
                alert(data.error);
              }
            });
          });
        }
      });
    }else{
      this.router.navigate(['/auth']);
    }
  }

  GetFriends(){
    this.friends=undefined;
    const token = this.authService.getToken();
    this.profileService.GetProfileInfo(token,this.email).subscribe((data:ProfileAbout)=>{
      if(data.success){
        this.friendService.GetFriends(token,data.email,0).subscribe((data:SearchResultObjects)=>{
          if(data.success){
            this.friends = data.results;
          }else{
            alert(data.error);
          }
        });
      }else{
        alert(data.error);
      }
    });
  }

  RemoveFriend(sr:SearchResultObject){
    const token = this.authService.getToken();
    this.friendService.RemoveFriend(token,sr.id).subscribe((data:FriendResponseObject)=>{
      if(data.success){
        this.GetFriends();
      }else{
        alert(data.error);
      }
    });
  }

}
