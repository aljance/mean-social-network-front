import { Component, OnInit, Inject } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { ValidateService } from '../../services/validate.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { PostService } from '../../services/post.service';
import { ProfileService } from '../../services/profile.service';
import { PictureService } from '../../services/picture.service';

export interface TokenObject{
  success:boolean
}

export interface PostMakeObject{
  success:boolean,
  error?:string,
  message?:string
}

export interface ProfileAbout{
  success:boolean,
  error?:string,
  first_name?:string,
  last_name?:string,
  email?:string,
  id?:number,
  profile_pic?:string
}

export interface CommentObject{
  id:number,
  like_count:number,
  upload_date:string,
  first_name:string,
  last_name:string,
  profile_pic:string,
  text:string,
  liked:boolean,
  email:string
}

export interface CommentObjects{
  success:boolean,
  error?:string,
  comments?:CommentObject[]
}

export interface PostLikeObject{
  success:boolean,
  error?:string,
  liked?:boolean
}

export interface PostObject{
  id:number,
  user_id:number,
  text:string,
  upload_date:string,
  deleted:boolean,
  like_count:number,
  comment_count:number,
  view_comment:boolean,
  liked:boolean,
  first_name:string,
  last_name:string,
  profile_pic:string,
  email:string
}

export interface PostObjects{
  success:boolean,
  error?:string,
  results?:PostObject[]
}

export interface Img{
  id:string,
  title?:string,
  description?:string,
  datetime:number,
  type:string,
  animated:boolean,
  width:number,
  height:number,
  size:number,
  views:number,
  bandwidth:number,
  vote?:number,
  favorite:boolean,
  nsfw?:string,
  section?:string,
  account_url?:string,
  account_id?:number,
  is_ad:boolean,
  in_most_viral:boolean,
  tags:string[],
  ad_type:number,
  ad_url?:string,
  in_gallery:boolean,
  deletehash:string,
  name?:string,
  link:string
}

export interface ImgurPost{
  success:boolean,
  status:number,
  data?:Img
}

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  private email:string;
  private posts:PostObject[];
  private post_text:string;
  private img_url:string;
  private comment_text:string;
  private comments:CommentObject[];

  constructor(
    private authService:AuthenticationService,
    private router:Router,
    private validateService:ValidateService,
    private activatedRoute:ActivatedRoute,
    private postService:PostService,
    private profileService: ProfileService,
    private picService:PictureService
  ) { }

  ngOnInit() {
    if(this.authService.checkTokenExists()){
      this.authService.checkToken().subscribe((data:TokenObject)=>{
        if(!data.success){
          this.router.navigate(['/auth']);
        }else{
          this.activatedRoute.paramMap.subscribe(params=>{
            this.email = params.get('email');
            if(this.email=="all"){
              const token = this.authService.getToken();
              this.postService.GetAllPosts(token,0).subscribe((data:PostObjects)=>{
                if(data.success){
                  console.log(data.results);
                  this.posts = data.results;
                }else{
                  alert(data.error);
                }
              });
            }else if(this.email==null){
              this.activatedRoute.parent.paramMap.subscribe(p=>{
                this.email = p.get('email');
                if(this.email=="me"){
                  const token = this.authService.getToken();
                  this.profileService.GetProfileInfo(token,this.email).subscribe((data:ProfileAbout)=>{
                    if(data.success){
                      this.email = data.email;
                      this.postService.GetPostsForUser(token,this.email,0).subscribe((data:PostObjects)=>{
                        if(data.success){
                          this.posts = data.results;
                        }else{
                          alert(data.error);
                        }
                      });
                    }else{
                      alert(data.error);
                    }
                  });
                }else{
                  const token = this.authService.getToken();
                  this.postService.GetPostsForUser(token,this.email,0).subscribe((data:PostObjects)=>{
                    if(data.success){
                      this.posts = data.results;
                    }else{
                      alert(data.error);
                    }
                  });
                }
              });
          }
        });
      }
      });
    }else{
      this.router.navigate(['/auth']);
    }
  }

  LikePost(post:PostObject){
    const token = this.authService.getToken();
    this.postService.LikePost(token,post.id).subscribe((data:PostLikeObject)=>{
      console.log(data);
      if(data.success){
        if(data.liked){
          post.like_count+=1;
        }else{
          post.like_count-=1;
        }
      }else{
        alert(data.error);
      }
    });
  }

  ViewComments(post:PostObject){
    console.log(post.view_comment);
    if(post.view_comment){
      post.view_comment=false;
    }else{
      post.view_comment=true;
      this.comments = undefined;
      const token = this.authService.getToken();
      this.postService.GetComments(token,post.id,0).subscribe((data:CommentObjects)=>{
        console.log(data);
        if(data.success){
          this.comments = data.comments;
        }else{
          alert(data.error);
        }
      });
    }
  }

  CommentPost(post:PostObject){
    const text = this.comment_text;
    this.comment_text = undefined;
    const token = this.authService.getToken();
    this.postService.MakeComment(token,text,post.id).subscribe((data:PostMakeObject)=>{
      if(data.success){
        post.comment_count+=1;
        alert('Comment posted');
      }else{
        alert(data.error);
      }
    });
  }

  LikeComment(comment:CommentObject){
    const token = this.authService.getToken();
    this.postService.LikeComment(token,comment.id).subscribe((data:PostLikeObject)=>{
      if(data.success){
        if(data.liked){
          comment.liked=true;
          comment.like_count+=1;
        }else{
          comment.liked=false;
          comment.like_count-=1;
        }
      }else{
        alert(data.error);
      }
    });
  }

  MakePost(){
    const text = this.post_text;
    this.post_text = undefined;
    const token = this.authService.getToken();
    this.postService.MakePost(token,text).subscribe((data:PostMakeObject)=>{
      if(data.success){
        alert(data.message);
        if(this.email=="all"){
          const token = this.authService.getToken();
          this.posts = undefined;
          this.postService.GetAllPosts(token,0).subscribe((data:PostObjects)=>{
            if(data.success){
              console.log(data.results);
              this.posts = data.results;
            }else{
              alert(data.error);
            }
          });
        }else{
          const token = this.authService.getToken();
          this.posts=undefined;
          this.postService.GetPostsForUser(token,this.email,0).subscribe((data:PostObjects)=>{
            if(data.success){
              this.posts = data.results;
            }else{
              alert(data.error);
            }
          });
        }
      }else{
        console.log(data.error);
      }
    });
  }

  PostImage(event:EventTarget){
    let eventObj: MSInputMethodContext = <MSInputMethodContext> event;
    let target: HTMLInputElement = <HTMLInputElement> eventObj.target;
    let files: FileList = target.files;
    let file: File = files[0];
    console.log(file);
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = ()=>{
      this.picService.UploadImage((reader.result as string).split(',')[1]).subscribe((data:ImgurPost)=>{
        console.log(data);
        if(data.success){
          const url = data.data.link;
          const token:string = this.authService.getToken();
          this.picService.SaveImage(token,url).subscribe((d:PostMakeObject)=>{
            if(d.success){
              alert('Image Uploaded');
            }else{
              alert(d.error);
            }
          });
        }else{
          alert('error uploading image');
        }
      });
    };
  }

  ViewProfile(email:string){
  }

}
