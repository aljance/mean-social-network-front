import { Component, OnInit } from '@angular/core';
import { SearchserviceService } from '../../services/searchservice.service';
import { AuthenticationService } from '../../services/authentication.service';
import { ValidateService } from '../../services/validate.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FriendService } from '../../services/friend.service';

export interface TokenObject{
  success:boolean
}

export interface FriendResponseObject{
  success:boolean,
  error?:string,
  message?:string
}

export interface SearchResultObject{
    first_name:string,
    last_name:string,
    id:number,
    email:string,
    profile_pic:string,
    friend_req_sent:boolean,
    friends:boolean,
    friend_req_received:boolean
}

export interface SearchResultsObject{
  success:boolean,
  error?:string,
  results?:SearchResultObject[]
}

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  private offset:number;
  private search:string;
  private results:SearchResultObject[];

  constructor(
    private authService:AuthenticationService,
    private router:Router,
    private searchService: SearchserviceService,
    private validateService: ValidateService,
    private activatedRoute: ActivatedRoute,
    private friendService: FriendService
  ) { }

  ngOnInit() {
    if(this.authService.checkTokenExists()){
      this.authService.checkToken().subscribe((data:TokenObject)=>{
        if(!data.success){
          this.router.navigate(['/auth']);
        }else{
          this.activatedRoute.paramMap.subscribe(params=>{
            this.offset = params.get('offset') as any as number;
            this.search = params.get('search');
            const token = this.authService.getToken();
            this.searchService.findUser(this.search,token,this.offset).subscribe((data:SearchResultsObject)=>{
              if(!data.success){
                alert('Search Returned No Results');
              }else{
                console.log(data.results);
                this.results = data.results;
              }
            });
          });
        }
      });
    }else{
      this.router.navigate(['/auth']);
    }
  }

  AddFriend(sr:SearchResultObject){
    const token = this.authService.getToken();
    this.friendService.SendFriendReq(token,sr.id).subscribe((data:FriendResponseObject)=>{
      if(data.success){
        sr.friend_req_sent=true;
      }else{
        alert(data.error);
      }
    });
  }

  RemoveFriend(sr:SearchResultObject){
    const token = this.authService.getToken();
    this.friendService.RemoveFriend(token,sr.id).subscribe((data:FriendResponseObject)=>{
      if(data.success){
        sr.friends=false;
      }else{
        alert(data.error);
      }
    });
  }

  ViewProfile(id:number){
  }

  CancelRequest(sr:SearchResultObject){
    const token = this.authService.getToken();
    this.friendService.CancelFriendReq(token,sr.id).subscribe((data:FriendResponseObject)=>{
      if(data.success){
        sr.friend_req_sent=false;
      }else{
        alert(data.error);
      }
    });
  }

  AcceptRequest(sr:SearchResultObject){
    const token = this.authService.getToken();
    this.friendService.AcceptReq(token,sr.id).subscribe((data:FriendResponseObject)=>{
      if(data.success){
        sr.friend_req_received=false;
        sr.friends=true;
      }else{
        alert(data.error);
      }
    });
  }

  DeclineRequest(sr:SearchResultObject){
    const token = this.authService.getToken();
    this.friendService.DeclineReq(token,sr.id).subscribe((data:FriendResponseObject)=>{
      if(data.success){
        sr.friend_req_received=false;
      }else{
        alert(data.error);
      }
    });
  }

}
