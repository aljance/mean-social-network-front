import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { SearchserviceService } from '../../services/searchservice.service';
import { ProfileService } from '../../services/profile.service';

export interface SearchResultsObject{
  success:boolean,
  error?:string,
  results?:string
}

export interface TokenObject{
  success:boolean
}

export interface LogOutObject{
  success:boolean,
  error?:string,
  message?:string
}

export interface ProfileAbout{
  success:boolean,
  error?:string,
  first_name?:string,
  last_name?:string,
  email?:string,
  id?:number,
  profile_pic?:string
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  private isCollapsed:boolean = true;
  private dropMenu:boolean = true;

  private search_text:string;
  private first_name:string;
  private last_name:string;

  constructor(
    private validateService: ValidateService,
    private authService: AuthenticationService,
    private searchService: SearchserviceService,
    private router: Router,
    private profileService:ProfileService
  ) {
  }

  ngOnInit() {
    if(this.authService.checkTokenExists()){
      this.authService.checkToken().subscribe((data:TokenObject)=>{
        if(!data.success){
          this.router.navigate(['/auth']);
        }else{
          const token = this.authService.getToken();
          this.profileService.GetProfileInfo(token,"me").subscribe((data:ProfileAbout)=>{
            if(data.success){
              this.first_name = data.first_name;
              this.last_name = data.last_name;
            }else{
              alert(data.error);
            }
          });
        }
      });
    }else{
      this.router.navigate(['/auth']);
    }
  }

  ViewMyProfile(){
    this.router.navigate(['home/profile/me']);
  }

  LogOut(){
    this.authService.logOut().subscribe((data:LogOutObject)=>{
      if(data.success){
        this.authService.destroyCookie();
        this.router.navigate(['/auth']);
      }else{
        alert(data.error);
      }
    });
  }

  SearchUser(){
    if(this.validateService.validateName(this.search_text)){
      const s = this.search_text;
      this.search_text = undefined;
      const token=this.authService.getToken();
      this.searchService.findUser(s,token,0).subscribe((data:SearchResultsObject)=>{
        if(!data.success){
          alert('Search Returned No Results');
        }else{
          this.router.navigate(['home/search/'+s+'/0']);
        }
      });
    }else{
      this.search_text = undefined;
      alert('Invalid Search Input. Try Again');
    }
  }

  ViewFriendReq(){
    this.router.navigate(['home/friend']);
  }

  ViewChat(){
    this.router.navigate(['home/chat']);
  }

  ViewNotif(){
    this.router.navigate(['home/notification']);
  }

}
