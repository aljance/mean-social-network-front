import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { ChatService } from '../../services/chat.service';

export interface TokenObject{
  success:boolean
}

export interface FriendObject{
  first_name:string,
  last_name:string,
  email:string,
  user_id:number,
  profile_pic:string,
  viewed:boolean
}

export interface FriendObjects{
  success:boolean,
  error?:string,
  results?:FriendObject[]
}

export interface Msg{
  upload_date:string,
  text:string
}

export interface MsgObjects{
  success:boolean,
  error?:string,
  first_name?:string,
  last_name?:string,
  user_id?:number,
  email?:string,
  chat_id?:number,
  profile_pic?:string,
  messages?:Msg[]
}

export interface ResponseObject{
  success:boolean,
  error?:string,
  message?:string
}

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  private friends:FriendObject[];
  private first_name:string;
  private last_name:string;
  private chat_id:number;
  private email:string;
  private user_id:number;
  private profile_pic:string;
  private msg:Msg[];
  private text:string;
  private selected_index:number=-1;

  constructor(
    private authService:AuthenticationService,
    private router:Router,
    private chatService:ChatService
  ) { }

  ngOnInit() {
    if(this.authService.checkTokenExists()){
      this.authService.checkToken().subscribe((data:TokenObject)=>{
        if(!data.success){
          this.router.navigate(['/auth']);
        }else{
          this.GetUsers();
        }
      });
    }else{
      this.router.navigate(['/auth']);
    }
  }

  GetUsers(){
    this.friends = undefined;
    const token = this.authService.getToken();
    this.chatService.GetUsers(token,0).subscribe((data:FriendObjects)=>{
      if(data.success){
        this.friends = data.results;
      }else{
        alert(data.error);
      }
    });
  }

  SelectUser(f:FriendObject){
    this.selected_index=f.user_id;
    this.GetMessagesFromUser(f.user_id);
  }

  GetMessagesFromUser(user_id:number){
    const token = this.authService.getToken();
    this.msg=undefined;
    this.chatService.GetMessagesFromUser(token,0,user_id).subscribe((data:MsgObjects)=>{
      console.log(data);
      if(data.success){
        this.first_name=data.first_name;
        this.last_name=data.last_name;
        this.email=data.email;
        this.user_id=data.user_id;
        this.chat_id=data.chat_id;
        this.profile_pic=data.profile_pic;
        this.msg=data.messages;
      }else{
        alert(data.error);
      }
    });
  }

  SendMessage(){
    const token = this.authService.getToken();
    const txt = this.text;
    this.text = undefined;
    console.log(this.user_id);
    this.chatService.SendMessage(token,txt,this.user_id).subscribe((data:ResponseObject)=>{
      if(data.success){
        this.GetMessagesFromUser(this.user_id);
      }else{
        alert(data.error);
      }
    });
  }

}
