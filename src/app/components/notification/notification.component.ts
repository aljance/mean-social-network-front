import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { NotifService } from '../../services/notif.service';

export interface TokenObject{
  success:boolean
}

export interface Notif{
  first_name:string,
  last_name:string,
  emali:string,
  profile_pic:string,
  notif_id:number,
  text:string
}

export interface Notifs{
  success:boolean,
  error?:string,
  results:Notif[]
}

export interface ResponseObject{
  success:boolean,
  error?:string,
  message?:string
}

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  private notif:Notif[];

  constructor(
    private authService:AuthenticationService,
    private router:Router,
    private notifService:NotifService
  ) { }

  ngOnInit() {
    if(this.authService.checkTokenExists()){
      this.authService.checkToken().subscribe((data:TokenObject)=>{
        if(!data.success){
          this.router.navigate(['/auth']);
        }else{
          this.GetNotifications();
        }
      });
    }else{
      this.router.navigate(['/auth']);
    }
  }

  GetNotifications(){
    const token = this.authService.getToken();
    this.notifService.GetNotifications(token,0).subscribe((data:Notifs)=>{
      if(data.success){
        this.notif = data.results;
      }else{
        alert(data.error);
      }
    });
  }

  ViewProfile(email:string){
    this.router.navigate(['home/profile/'+email]);
  }

  ReadNotification(id:number){
    console.log(id);
    const token = this.authService.getToken();
    this.notifService.ReadNotification(token,id).subscribe((data:ResponseObject)=>{
      if(data.success){
        this.GetNotifications();
      }else{
        alert(data.error);
      }
    });
  }

}
