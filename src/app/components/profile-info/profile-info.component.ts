import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { ProfileService } from '../../services/profile.service';

export interface TokenObject{
  success:boolean
}

export interface ResponseObject{
  success:boolean,
  error?:string,
  message?:string
}

export interface ProfileAbout{
  success:boolean,
  error?:string,
  first_name?:string,
  last_name?:string,
  email?:string,
  id?:number,
  profile_pic?:string
}

@Component({
  selector: 'app-profile-info',
  templateUrl: './profile-info.component.html',
  styleUrls: ['./profile-info.component.css']
})
export class ProfileInfoComponent implements OnInit {

  private first_name:string;
  private last_name:string;
  private email:string;
  private myprofile:boolean=false;
  private old_password:string;
  private new_password:string;
  private confirm_password:string;

  constructor(
    private authService:AuthenticationService,
    private router:Router,
    private activatedRoute:ActivatedRoute,
    private profileService: ProfileService
  ) { }

  ngOnInit() {
    if(this.authService.checkTokenExists()){
      this.authService.checkToken().subscribe((data:TokenObject)=>{
        if(!data.success){
          this.router.navigate(['/auth']);
        }else{
          this.activatedRoute.parent.paramMap.subscribe(p=>{
            const email = p.get('email');
            const token = this.authService.getToken();
            this.profileService.GetProfileInfo(token,email).subscribe((data:ProfileAbout)=>{
              if(data.success){
                this.first_name = data.first_name;
                this.last_name = data.last_name;
                this.email = data.email;
                console.log(email);
                console.log(this.email);
                if(email=="me"){
                  this.myprofile=true;
                }else{
                  this.myprofile=false;
                }
              }else{
                alert(data.error);
              }
            });
          });
        }
      });
    }else{
      this.router.navigate(['/auth']);
    }
  }

  UpdateInfo(){
    const token = this.authService.getToken();
    this.profileService.UpdateProfile(token,this.email,this.first_name,this.last_name).subscribe((data:ResponseObject)=>{
      if(data.success){
        alert('Information Updated');
      }else{
        alert(data.error);
      }
    });
  }

  DeleteAccount(){
    const token = this.authService.getToken();
    this.profileService.DeleteAccount(token).subscribe((data:ResponseObject)=>{
      if(data.success){
        this.authService.destroyCookie();
        this.router.navigate(['/auth']);
      }else{
        alert(data.error);
      }
    });
  }

  ChangePassword(){
    const token = this.authService.getToken();
    const old_password = this.old_password;
    const new_password = this.new_password;
    const confirm_password = this.confirm_password;
    this.old_password=undefined;
    this.new_password=undefined;
    this.confirm_password=undefined;
    if(new_password!=confirm_password){
      alert('Passwords do not match');
    }else if(old_password==new_password){
      alert('New Password can\'t be old password');
    }else{
      this.profileService.ChangePassword(token,old_password,new_password,confirm_password).subscribe((data:ResponseObject)=>{
        console.log(data);
        if(data.success){
          this.authService.destroyCookie();
          this.router.navigate(['/auth']);
        }else{
          alert(data.error);
        }
      });
    }
  }

}
