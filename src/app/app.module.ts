//modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AngularWebStorageModule } from 'angular-web-storage';
import { AppRoutingModule } from './app-routing.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { CollapseModule } from 'ngx-bootstrap/collapse';

//components
import { AppComponent } from './app.component';
import { AuthComponent } from './components/auth/auth.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { SearchComponent } from './components/search/search.component';
import { NotificationComponent } from './components/notification/notification.component';
import { FriendComponent } from './components/friend/friend.component';
import { ChatComponent } from './components/chat/chat.component';
import { PostComponent } from './components/post/post.component';
import { ProfileInfoComponent } from './components/profile-info/profile-info.component';
import { ProfileFriendComponent } from './components/profile-friend/profile-friend.component';
import { PictureComponent } from './components/picture/picture.component';

//services
import { ValidateService } from './services/validate.service';
import { AuthenticationService } from './services/authentication.service';
import { SearchserviceService } from './services/searchservice.service';
import { FriendService } from './services/friend.service';
import { PostService } from './services/post.service';
import { ProfileService } from './services/profile.service';
import { PictureService } from './services/picture.service';
import { NotifService } from './services/notif.service';
import { ChatService } from './services/chat.service';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    NavbarComponent,
    HomeComponent,
    ProfileComponent,
    SearchComponent,
    NotificationComponent,
    FriendComponent,
    ChatComponent,
    PostComponent,
    ProfileInfoComponent,
    ProfileFriendComponent,
    PictureComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    CollapseModule.forRoot(),
    FormsModule,
    HttpClientModule,
    AngularWebStorageModule
  ],
  providers: [
    ValidateService,
    AuthenticationService,
    SearchserviceService,
    FriendService,
    PostService,
    ProfileService,
    PictureService,
    NotifService,
    ChatService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
