import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthComponent } from './components/auth/auth.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { SearchComponent } from './components/search/search.component';
import { NotificationComponent } from './components/notification/notification.component';
import { FriendComponent } from './components/friend/friend.component';
import { ChatComponent } from './components/chat/chat.component';
import { PostComponent } from './components/post/post.component';
import { ProfileFriendComponent } from './components/profile-friend/profile-friend.component';
import { ProfileInfoComponent } from './components/profile-info/profile-info.component';
import { PictureComponent } from './components/picture/picture.component';

const routes: Routes = [
  { path: '', redirectTo: '/auth', pathMatch: 'full' },
  { path: 'auth', component: AuthComponent },
  { path: 'home', component: HomeComponent, children:[
    { path: 'profile/:email', component: ProfileComponent, children:[
      { path: 'post', component: PostComponent },
      { path: 'about', component: ProfileInfoComponent },
      { path: 'picture', component: PictureComponent },
      { path: 'friends', component: ProfileFriendComponent }
    ] },
    { path: 'search/:search/:offset', component: SearchComponent },
    { path: 'friend', component: FriendComponent },
    { path: 'notification', component: NotificationComponent },
    { path: 'chat', component: ChatComponent },
    { path: 'post/:email', component: PostComponent }
  ] },
  { path: '**', redirectTo: '/auth'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
