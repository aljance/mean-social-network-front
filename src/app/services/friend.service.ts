import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FriendService {

  constructor(
    private http:HttpClient
  ) { }

  GetFriends(token:string,email:string,offset:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    return this.http.get('http://localhost:3000/api/friends/'+email+'/all/'+offset,{headers:headers})
    .pipe(map(res=>res));
  }

  SendFriendReq(token:string,id:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    const body = {
      friend_id:id
    };
    return this.http.post('http://localhost:3000/api/friends/add',body,{headers:headers})
    .pipe(map(res=>res));
  }

  CancelFriendReq(token:string,id:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    const body = {
      friend_id:id
    };
    return this.http.post('http://localhost:3000/api/friends/cancel',body,{headers:headers})
    .pipe(map(res=>res));
  }

  RemoveFriend(token:string,id:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    const body = {
      friend_id:id
    };
    return this.http.post('http://localhost:3000/api/friends/delete',body,{headers:headers})
    .pipe(map(res=>res));
  }

  AcceptReq(token:string,id:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    const body = {
      friend_id:id
    };
    return this.http.post('http://localhost:3000/api/friends/accept',body,{headers:headers})
    .pipe(map(res=>res));
  }

  DeclineReq(token:string,id:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    const body = {
      friend_id:id
    };
    return this.http.post('http://localhost:3000/api/friends/decline',body,{headers:headers})
    .pipe(map(res=>res));
  }

  GetFriendReq(token:string,offset:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    return this.http.get('http://localhost:3000/api/friends/request/all/'+offset,{headers:headers})
    .pipe(map(res=>res));
  }
}
