import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(
    private http:HttpClient
  ) { }

  GetProfileInfo(token:string,email:string){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    return this.http.get('http://localhost:3000/api/users/'+email+'/info',{headers:headers})
    .pipe(map(res=>res));
  }

  UpdateProfile(token:string,email:string,first_name:string,last_name:string){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    const body = {
      'first_name':first_name,
      'last_name':last_name
    };
    return this.http.post('http://localhost:3000/api/users/update',body,{headers:headers})
    .pipe(map(res=>res));
  }

  DeleteAccount(token:string){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    const body = {};
    return this.http.post('http://localhost:3000/api/users/delete',body,{headers:headers})
    .pipe(map(res=>res));
  }

  ChangePassword(token:string,old_password:string,new_password:string,confirm_password:string){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    const body = {
      'old_password':old_password,
      'new_password':new_password,
      'confirm_password':confirm_password
    };
    return this.http.post('http://localhost:3000/api/users/password/change',body,{headers:headers})
    .pipe(map(res=>res));
  }

}
