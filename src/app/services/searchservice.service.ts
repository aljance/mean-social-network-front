import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SearchserviceService {

  constructor(
    private http:HttpClient
  ) { }

  findUser(search:string,token:string,offset:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    return this.http.get('http://localhost:3000/api/users/find/'+search+'/'+offset,{headers:headers})
    .pipe(map(res=>res));
  }
}
