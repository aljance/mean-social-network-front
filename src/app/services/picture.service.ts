import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PictureService {

  private imgur_access_token:string = "3048e3b34d3032f2d3b0ade5fd8ddfd96c8346e9";

  constructor(
    private http:HttpClient
  ) { }

  GetUserPictures(token:string,email:string,offset:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    return this.http.get('http://localhost:3000/api/pictures/'+email+'/all/'+offset,{headers:headers})
    .pipe(map(res=>res));
  }

  SaveImage(token:string,url:string){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    const body = {
      pic_url:url
    };
    return this.http.post('http://localhost:3000/api/pictures/create',body,{headers:headers})
    .pipe(map(res=>res));
  }

  UploadImage(file:string){
    let headers = new HttpHeaders({
      'Content-Type':'application/x-www-form-urlencoded',
      'Authorization':'Bearer '+this.imgur_access_token
    });
    return this.http.post('https://api.imgur.com/3/image',file,{headers:headers})
    .pipe(map(res=>res));
  }

  SetProfilePic(token:string,id:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    const body = {
      pic_id:id
    };
    return this.http.post('http://localhost:3000/api/pictures/profile_pic',body,{headers:headers})
    .pipe(map(res=>res));
  }

  DeleteImage(token:string,id:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    const body = {
      pic_id:id
    };
    return this.http.post('http://localhost:3000/api/pictures/delete',body,{headers:headers})
    .pipe(map(res=>res));
  }
}
