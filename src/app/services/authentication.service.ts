import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SessionStorageService, SessionStorage } from 'angular-web-storage';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  authToken: any;
  user: any;

  constructor(
    private http:HttpClient,
    private session: SessionStorageService,
  ) { }

  setToken(token:string): void{
    this.authToken = token;
    this.session.set('Token',{value:token},1,'d');
  }

  checkToken(){
    this.authToken = this.getToken();
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+this.authToken
    });
    return this.http.get('http://localhost:3000/api/users/check_token',{headers:headers})
    .pipe(map(res=> res));
  }

  checkTokenExists(){
    const val= this.session.get('Token');
    if(val==null){
      return false;
    }else{
      return true;
    }
  }

  destroyCookie(){
    this.authToken=undefined;
    this.user=undefined;
    this.session.clear();
  }

  getToken():string{
    return this.session.get('Token').value;
  }

  logOut(){
    this.authToken = this.getToken();
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+this.authToken
    });
    return this.http.get('http://localhost:3000/api/users/logout',{headers:headers})
    .pipe(map(res=>res));
  }

  registerUser(user){
    let headers = new HttpHeaders();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/api/users/register',user,{headers:headers})
    .pipe(map(res=> res));
  }

  loginUser(user){
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.http.post('http://localhost:3000/api/users/login',user,{headers:headers})
    .pipe(map(res=>res));
  }
}
