import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NotifService {

  constructor(
    private http:HttpClient
  ) { }

  GetNotifications(token:string,offset:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    return this.http.get('http://localhost:3000/api/notif/all/'+offset,{headers:headers})
    .pipe(map(res=>res));
  }

  ReadNotification(token:string,id:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    const body = {
      notif_id:id
    };
    return this.http.post('http://localhost:3000/api/notif/update',body,{headers:headers})
    .pipe(map(res=>res));
  }
}
