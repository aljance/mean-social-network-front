import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidateService {

  constructor() { }

  validateRegister(user){
    if(user.first_name == undefined || user.last_name == undefined ||
    user.email == undefined || user.password == undefined || user.confirm_password == undefined){
      alert('Please Fill All Fields');
      return false;
    }else if(!this.validateName(user.first_name) || !this.validateName(user.last_name)){
      alert('Invalid Name');
      return false;
    }else if(!this.validateEmail(user.email)){
      alert('Invalid Email. Please Try Again.');
      return false;
    }else if(!this.validatePassword(user.password)){
      alert('Invalid Password. Please Try Again');
      return false;
    }else if(user.password!=user.confirm_password){
      alert('Passwords Don\'t Match. Try Again');
      return false;
    }else{
      return true;
    }
  }

  validateName(name:string): boolean{
    const re = /([A-Za-z])/
    return re.test(name);
  }

  validatePassword(password:string):boolean{
    const re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
    return re.test(password);
  }

  validateLogin(user):boolean{
    if(user.email==undefined || user.password==undefined){
      alert('Please Fill All Fields');
      return false;
    }else if(!this.validateEmail(user.email)){
      alert('Invalid Email. Please Try Again');
      return false;
    }else if(!this.validatePassword(user.password)){
      alert('Invalid Password. Please Try Again');
      return false;
    }else{
      return true;
    }
  }

  validateEmail(email:string): boolean{
    const re = /([A-Za-z0-9]+@[A-Za-z0-9]+\.[a-z]+)/;
    return re.test(email);
  }
}
