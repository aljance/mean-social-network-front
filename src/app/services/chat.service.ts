import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(
    private http:HttpClient
  ) { }

  GetUsers(token:string,offset:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    return this.http.get('http://localhost:3000/api/chat/all/friends/'+offset,{headers:headers})
    .pipe(map(res=>res));
  }

  GetMessagesFromUser(token:string,offset:number,user:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    return this.http.get('http://localhost:3000/api/chat/all/'+user+'/'+offset,{headers:headers})
    .pipe(map(res=>res));
  }

  SendMessage(token:string,msg:string,receiver:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    const body = {
      text:msg,
      receiver_id:receiver
    };
    return this.http.post('http://localhost:3000/api/chat/send',body,{headers:headers})
    .pipe(map(res=>res));
  }
}
