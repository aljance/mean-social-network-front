import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(
    private http:HttpClient
  ) { }

  GetAllPosts(token:string,offset:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    return this.http.get('http://localhost:3000/api/posts/all/'+offset,{headers:headers})
    .pipe(map(res=>res));
  }

  GetPostsForUser(token:string,id:string,offset:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    return this.http.get('http://localhost:3000/api/posts/user/'+id+'/'+offset,{headers:headers})
    .pipe(map(res=>res));
  }

  MakePost(token:string,text:string){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    const body = {
      post_text:text
    };
    console.log(body);
    return this.http.post('http://localhost:3000/api/posts/create',body,{headers:headers})
    .pipe(map(res=>res));
  }

  MakeComment(token:string,text:string,post_id:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    const body = {
      text:text,
      post_id:post_id
    };
    return this.http.post('http://localhost:3000/api/comments/create',body,{headers:headers})
    .pipe(map(res=>res));
  }

  PostImage(token:string,img_url:string){

  }

  GetComments(token:string,post_id:number,offset:number){
    let headers = new HttpHeaders({
      'Content-Type':'applcation/json',
      'Authorization':'bearer '+token
    });
    return this.http.get('http://localhost:3000/api/comments/'+post_id+'/all/'+offset,{headers:headers})
    .pipe(map(res=>res));
  }

  LikePost(token:string,post_id:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    const body = {
      post_id:post_id
    };
    return this.http.post('http://localhost:3000/api/posts/like',body,{headers:headers})
    .pipe(map(res=>res));
  }

  LikeComment(token:string,comment_id:number){
    let headers = new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'bearer '+token
    });
    const body = {
      comment_id:comment_id
    };
    return this.http.post('http://localhost:3000/api/comments/like',body,{headers:headers})
    .pipe(map(res=>res));
  }

  UpdatePost(token:string,post_id:number,text:string){
    let headers = new HttpHeaders({
      'Content-Type':'applcation/json',
      'Authorization':'bearer '+token
    });
    const body = {
      post_id:post_id,
      text:text
    };
    return this.http.post('http://localhost:3000/api/posts/update',body,{headers:headers})
    .pipe(map(res=>res));
  }

  UpdateComment(token:string,comment_id:number,text:string){
    let headers = new HttpHeaders({
      'Content-Type':'applcation/json',
      'Authorization':'bearer '+token
    });
    const body = {
      comment_id:comment_id,
      text:text
    };
    return this.http.post('http://localhost:3000/api/comments/update',body,{headers:headers})
    .pipe(map(res=>res));
  }

  DeletePost(token:string,post_id:number){
    let headers = new HttpHeaders({
      'Content-Type':'applcation/json',
      'Authorization':'bearer '+token
    });
    const body = {
      post_id:post_id
    };
    return this.http.post('http://localhost:3000/api/posts/delete',body,{headers:headers})
    .pipe(map(res=>res));
  }

  DeleteComment(token:string,comment_id:number){
    let headers = new HttpHeaders({
      'Content-Type':'applcation/json',
      'Authorization':'bearer '+token
    });
    const body = {
      comment_id:comment_id
    };
    return this.http.post('http://localhost:3000/api/comments/delete',body,{headers:headers})
    .pipe(map(res=>res));
  }
}
